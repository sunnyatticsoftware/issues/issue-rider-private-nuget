﻿using System;

namespace Sample
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is a test that uses a nuget dependency's function");
            var test = new Sasw.Sample.Library.Sample();
            Console.WriteLine(test.Test());
        }
    }
}