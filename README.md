# issue-rider-private-nuget

Sample project that publishes a nuget package to a private repository which needs nuget credentials.
https://youtrack.jetbrains.com/issue/RIDER-18949

## The issue
I've got a private nuget repository here https://gitlab.com/sunnyatticsoftware/sandbox/sample-private-nuget-repository (it's a private project so access is restricted).

It hosts some nuGet packages that can be read with proper nuGet credentials such as:
```
<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <packageSources>
    <add key="GitLab" value="https://gitlab.com/api/v4/projects/25707127/packages/nuget/index.json" />
  </packageSources>
  <packageSourceCredentials>
    <GitLab>
     <add key="Username" value="gitlab+deploy-token-416033" />
     <add key="ClearTextPassword" value="nx2-H14R4cZxtJpwUALV" />
    </GitLab>
  </packageSourceCredentials>
</configuration>
```

To reproduce, open the solution and go to Nuget Package management to browse repository GitLab that should be available
as there is a `NuGet.Config` adding it as a source.

The issue is that sometimes it prompts for credentials instead of reading them from `NuGet.Config`.
Other times it shows the packages being used but it cannot find new packages within the private repo.

As an example, open the nuget package manager UI in Rider and search for `Sasw.Sample.AnotherLibrary`. If it does not find it, there is a problem.

Also, try upgrading the existing `Sasw.Sample.Library` version

The private nuGet repository has the following packages available.
![Repository snapshot](repo.PNG)